<?php
ob_start();
 session_start();
  include_once("yhteysTietokantaan.php");
 // if session is not set this will redirect to login page
 if( !isset($_SESSION['user']) ) {
  header("Location: index.php");
  exit;
 }
$connect = connect_database();
 // select loggedin users detail
 $res=mysqli_query($connect,"SELECT * FROM users WHERE userId=".$_SESSION['user']);
 $userRow=mysqli_fetch_array($res);
?>
    <!DOCTYPE html>
    <?php
//include once toiminto koska muuten ei anna käyttää samaa koodia uudelleen. Huomattu kantapään kautta.
include("asukasTiedot.php");
?>
        <?php
include_once("varausTiedot.php");
$now = date("Y-m-d");
if (isset($_GET['selected_date']))
{
 $selected_date = $_GET["selected_date"]; 
} else {
 $selected_date = $now;
}
$reservations = getReservations($selected_date);
?>
            <html>

            <head>
                <meta charset="utf-8">
                <title>Mikkolantie saunavuoro varaus. Tervetuloa -
                    <?php echo $userRow['userEmail']; ?>
                </title>
                <!-- Bootstrap-->
                <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                <!--oma tyylitiedosto ja alla font-awesome iconeille linkki-->
                <link rel="stylesheet" type="text/css" href="style.css" />
            </head>

            <body>
                <div class="container">
                    <span class="glyphicon glyphicon-user"></span>&nbsp;Terve
                    <?php echo $userRow['userEmail']; ?>&nbsp;
                    <a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a>
                    <h1>Saunavuoron varaus</h1>
                    <div class="form-group form-horizontal">
                        <?php
                    if (isset($alert)){
                        echo '<div class="alert alert-danger">
                        <strong>!</strong>'.$alert.'
                        </div>';
                        }
                    ?>

                            <!-- form päivämäärän ja ajan lisäys tietokantaan.-->
                            <form action="varausTallennus.php" method="post">
                                <br>
                                <div class="element">
                                    Päivävalinta:
                                    <input type="date" name="Reservationdate" value="<?php echo $selected_date;?>" required/>
                                </div>
                                <br>
                                <div class="element">
                                    Valitse aloitusaika:
                                    <!--required toiminnolla rajaus ajanvaraukseen-->
                                    <input type="time" name="Starttime" required min="10:00" max="22:00" />
                                </div>
                                <div class="element">
                                    <br> Valitse lopetusaika
                                    <input type="time" name="Endtime" required min="10:00" max="23:00" />
                                    <input type="submit" />
                                </div>
                                <br>
                                <div class="element">
                                    <h2>Varaukset</h2>
                                    <div class="after-box">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>Käyttäjä</th>
                                                    <th>Pvm</th>
                                                    <th>Alkaa</th>
                                                    <th>Loppuu</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
    foreach ($reservations as $reservation) {
        echo  '<tr><td>'.$reservation["userName"].'</td><td>'.$reservation["Reservationdate"].'</td><td>'.$reservation["Starttime"].'</td><td>'.$reservation["Endtime"].'</td></tr>';
}
 ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>

                <!-- jQuery  -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                <!-- Include all compiled plugins (below), or include individual files as needed -->
                <script src="bootstrap/dist/js/bootstrap.min.js"></script>
                <script>
                    $('[name="Reservationdate"]').on('change', function(event) {
                        window.location.href = window.location.origin + window.location.pathname + "?" + $.param({
                            selected_date: $(this).val()
                        });
                    })

                </script>
            </body>

            </html>
            <?php ob_end_flush();?>
