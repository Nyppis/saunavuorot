<?php
ob_start();
 session_start();
//Tietokantayhteys
include_once("yhteysTietokantaan.php");
include_once("varausTiedot.php");
$connect = connect_database();
//Määritellyt muuttujat tarvittavia komentoja varten.
$datenow = date("Y-m-d");
$timenow = time();
$user_id = $_SESSION['user'];
$reservationdate = $_POST['Reservationdate'];
$starttime = $_POST['Starttime'];
$endtime = $_POST['Endtime'];
$reservations = getReservations($reservationdate);
$stringstart = strtotime($starttime); 
$stringend = strtotime($endtime);
$reservationerror = "";
    
foreach ($reservations as $reservation) {
   $reservationstart = strtotime($reservation["Starttime"]); $reservationend = strtotime($reservation["Endtime"]);
    if((($stringstart >= $reservationstart) && ($stringstart < $reservationend)) || 
        (($stringend <= $reservationstart) && ($stringend >= $reservationend)) || 
        (($stringstart < $reservationstart) && ($stringend > $reservationend))) {
           $reservationerror = "Virhe! Varaukset eivät voi olla päällekkäin.";
        } 
}

//if ehdossa määritetään että jos aikavaraus(alkuaika) on isompi kun loppuaika niin tulostaa virheilmoituksen. Seuraava ehto ettei voi tehdä varausta menneisyyteen. kolmas ehto aloitusaika piti muuttaa strtotime muotoon jotta voi verrata aikaa, ettei voi tehdä menneisyyteen.

if ((strlen($reservationerror) != 0) || 
    ($starttime >= $endtime) ||
    ($reservationdate <= $datenow) &&
    ($stringstart < $timenow)) {     
    echo "Tarkista varaus!";
} else {
    $sql = "INSERT INTO reservation(Reservationdate, Starttime, Endtime, userId) VALUES('$reservationdate','$starttime','$endtime',$user_id);";
    //Jos yhteys tietokantaan toimi, palauttaa kyselyn sivulle index.php
    if($connect->query($sql) == true){
     header("Location:index.php");
    } else {
        echo "Varauksen lisäämisessä tuli virhe: ".$sql."<br>".$connect->error;
    }
}
//Suljetaan tietokantayhteys
$connect->close();
?>
 <?php ob_end_flush();?>
