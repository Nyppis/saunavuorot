<?php
//Tämä funktio ottaa yhteyden tietokantaan
function connect_database(){
/*Määritellään muuttujat palvelin, kayttajatunnus, salasana, tietokanta ja yhteys joita tarvitaan kun otetaan yhteys tietokantaan*/
$palvelin = "localhost";
$kayttajatunnus = "root";
$salasana = "";
$tietokanta = "sauna";
//Luodaan yhteys tietokantaan.
$connect = mysqli_connect($palvelin, $kayttajatunnus, $salasana, $tietokanta);
//Tarkistetaan yhteys
if(!$connect){
	die("Yhteys tietokantaan epäonnistui: ".mysqli_connect_error());
}
//Palautetaan yhteys
return $connect;
}
?>