<?php
ob_start();
include_once("yhteysTietokantaan.php");
 session_start();
 if( isset($_SESSION['user'])!="" ){
  header("Location: home.php");
 }
 $connect = connect_database();
 $error = false;
$name = "";
$email = "";
$pass = "";
$nameError = "";
$emailError = "";
$passError = ""; 
$query = "";

 if ( isset($_POST['btn-signup']) ) {
  
  // clean user inputs to prevent sql injections
  $name = trim($_POST['name']);
  $name = strip_tags($name);
  $name = htmlspecialchars($name);
  
  $email = trim($_POST['email']);
  $email = strip_tags($email);
  $email = htmlspecialchars($email);
  
  $pass = trim($_POST['pass']);
  $pass = strip_tags($pass);
  $pass = htmlspecialchars($pass);
  
  // basic name validation
  if (empty($name)) {
   $error = true;
   $nameError = "Kirjoita koko nimesi.";
  } else if (strlen($name) < 3) {
   $error = true;
   $nameError = "Nimessä pitää olla vähintään 3 kirjainta.";
  } else if (!preg_match("/^[a-zA-Z ]+$/",$name)) {
   $error = true;
   $nameError = "Nimessäsi kuuluu olla kirjaimia ja välilyönti välissä.";
  }
  
  //basic email validation
  if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
   $error = true;
   $emailError = "Syötä oikea s-postiosoite.";
  } else {
   // check email exist or not
   $query = "SELECT userEmail FROM users WHERE userEmail='$email'";
   $result = mysqli_query($connect, $query);
   $count = mysqli_num_rows($result);
   if($count!=0){
    $error = true;
    $emailError = "Antamasi salasana on jo käytössä.";
   }
  }
  // password validation
  if (empty($pass)){
   $error = true;
   $passError = "Ole hyvä ja syötä salasana.";
  } else if(strlen($pass) < 6) {
   $error = true;
   $passError = "Salasanassa tulee olla vähintään 6 merkkiä.";
  }
  
  // password encrypt using SHA256();
  $password = hash('sha256', $pass);
  
  // if there's no error, continue to signup
  if( !$error ) {
   
   $query = "INSERT INTO users(userName,userEmail,userPass) VALUES('$name','$email','$password')";
   $res = mysqli_query($connect, $query);
    
   if ($res) {
    $errTyp = "Kirjautuminen onnistui";
    $errMSG = "Kirjautuminen onnistui, voit nyt kirjautua sivustolle";
   $name = "";
$email = "";
$pass = "";
   } else {
    $errTyp = "Virhe";
    $errMSG = "Jotain meni väärin, yritä uudelleen kirjautumista myöhemmin..."; 
   } 
  }
 }
  $connect->close();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Mikkolantie saunavuorot- Kirjautuminen ja rekisteröityminen </title>
<link  href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

<div class="container">

 <div id="login-form">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">
    
     <div class="col-md-12">
        
         <div class="form-group">
             <h2 class="">Tiedot rekisteröitymistä varten</h2>
            </div>
        
         <div class="form-group">
             <hr />
            </div>
            
            <?php
   if ( isset($errMSG) ) {
    
    ?>
    <div class="form-group">
             <div class="alert alert-<?php echo ($errTyp=="Kirjautuminen onnistui") ? "Kirjautuminen onnistui" : $errTyp; ?>">
    <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                </div>
             </div>
                <?php
   }
   ?>
            
            <div class="form-group">
             <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
             <input type="text" name="name" class="form-control" placeholder="Syötä nimi" maxlength="50" value="<?php echo $name ?>" />
                </div>
                <span class="text-danger"><?php echo $nameError; ?></span>
            </div>
            
            <div class="form-group">
             <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
             <input type="email" name="email" class="form-control" placeholder="Syötä E-mail/sähköposti" maxlength="40" value="<?php echo $email ?>" />
                </div>
                <span class="text-danger"><?php echo $emailError; ?></span>
            </div>
            
            <div class="form-group">
             <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
             <input type="password" name="pass" class="form-control" placeholder="Syötä salasana" maxlength="15" />
                </div>
                <span class="text-danger"><?php echo $passError; ?></span>
            </div>
            
            <div class="form-group">
             <hr />
            </div>
            
            <div class="form-group">
             <button type="submit" class="btn btn-block btn-primary" name="btn-signup">Kirjaudu</button>
            </div>
            
            <div class="form-group">
             <hr />
            </div>
            
            <div class="form-group">
             <a href="index.php">Kirjaudu sisään täällä...</a>
            </div>
        </div>
    </form>
    </div> 
</div>

</body>
</html>
<?php ob_end_flush();?>